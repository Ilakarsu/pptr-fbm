const puppeteer = require('puppeteer');

// TODO only for DEV
require('dotenv').config();

const sleep = async (ms) => {
	return new Promise((res, rej) => {
		setTimeout(() => {
			res();
		}, ms);
	});
};

const ID = {
	login: '#email',
	pass: '#pass',
};

const args = [
	'--disable-background-timer-throttling',
	'--disable-breakpad',
	'--disable-client-side-phishing-detection',
	'--disable-cloud-import',
	'--disable-default-apps',
	'--disable-dev-shm-usage',
	'--disable-extensions',
	'--disable-gesture-typing',
	'--disable-hang-monitor',
	'--disable-infobars',
	'--disable-notifications',
	'--disable-offer-store-unmasked-wallet-cards',
	'--disable-offer-upload-credit-cards',
	'--disable-popup-blocking',
	'--disable-print-preview',
	'--disable-prompt-on-repost',
	'--disable-setuid-sandbox',
	'--disable-speech-api',
	'--disable-sync',
	'--disable-tab-for-desktop-share',
	'--disable-translate',
	'--disable-voice-input',
	'--disable-wake-on-wifi',
	'--enable-async-dns',
	'--enable-simple-cache-backend',
	'--enable-tcp-fast-open',
	'--enable-webgl',
	'--full-memory-crash-report',
	'--unlimited-storage',
	'--hide-scrollbars',
	'--ignore-gpu-blacklist',
	'--media-cache-size=33554432',
	'--metrics-recording-only',
	'--mute-audio',
	'--no-default-browser-check',
	'--no-first-run',
	'--no-pings',
	'--no-sandbox',
	'--no-zygote',
	'--password-store=basic',
	'--prerender-from-omnibox=disabled',
	'--use-gl=swiftshader',
	'--use-mock-keychain',
];

const defaultPuppeteerOptions = {
	args: args,
	pipe: true,
	defaultViewport: null,
	devtools: false,
	headless: true,
	ignoreHTTPSErrors: false,
	slowMo: 0,
};

const defaultViewport = {
	deviceScaleFactor: 1,
	hasTouch: false,
	height: 1024,
	isLandscape: false,
	isMobile: false,
	width: 1280,
};

(async () => {
	const browser = await puppeteer.launch(defaultPuppeteerOptions);
	const page = await browser.newPage();
	try {
		await page.setViewport(defaultViewport);

		page.on('console', (...args) => console.info('PAGE LOG:', ...args));

		page.on('error', (err) => {
			console.error(`Error event emitted: ${err}`);
			console.error(err.stack);
			browser.close();
		});

		console.log(`Loading page.. ${process.env.FB_LOGIN_URI}`);

		let login = async () => {
			// login
			await page.goto(process.env.FB_LOGIN_URI, { waitUntil: 'networkidle2' });
			console.log('page loaded');
			await page.waitForSelector(ID.login);
			await page.type(ID.login, process.env.USER_EMAIL);
			console.log(
				`Filling data for: ${ID.login} field with ${process.env.USER_EMAIL}`
			);

			await page.type(ID.pass, process.env.USER_PW);
			await sleep(500);

			await page.click('#loginbutton');

			console.log('login done');
			await page.waitForNavigation();
		};

		await login();

		// navigate to mrktplace
		await sleep(600);
		// await page.click('#1606854132932955');
		await page.goto('https://www.facebook.com/marketplace/?ref=bookmark', {
			waitUntil: 'networkidle2',
		});

		await page.screenshot({
			path: 'facebook.png',
		});

		console.log('..closing browser.');
		page.close();
		browser.close();
	} catch (error) {
		browser.close();
		console.log(`ERROR: ${error}`);
	}
})();
