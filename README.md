# README #

Prototype to showcase the scraping functionality of pptr on facebook marketplace.

## What is this repository for ##

* pptr-fbm 1.0

## How do I get set up ##

You can use any IDE of choice, however my recommendation is VSC (Visual Studio Code).

### Configuration ###

* USER_EMAIL, USER_PW and FB_LOGIN_URI needs to be provided. See example .env file:

```env
    FB_LOGIN_URI = https://facebook.com
    USER_EMAIL = dummy@mail.com
    USER_PW = dummy
```

### Dependencies ###

* nodejs
* npm || yarn
* puppeteer
* dotenv

### Database configuration ###

* n/a

### How to run tests ###

* Set credentials in .env file and run: npm start.
* **LINUX ONLY**: First time, run the following command:

```bash
    sudo apt-get update
    sudo apt-get install -y libgbm-dev
```

### Deployment instructions ###

* n/a

## TODOS ##

### Misc ###

* Randomzie sleep timer as FB is likely to detect constant sleep timers and classify it as a bot.
* Go to marketplace using FB UI buttons, e.g. ID of button element "Marketplace" -> mimics user behaviour more accurately.
* Refactoring / Styling etc.
